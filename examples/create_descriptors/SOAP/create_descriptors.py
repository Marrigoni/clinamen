# -*- coding: utf-8 -*-
""" Copyright 2020 Marco Arrigoni

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np

import ase.build
from ase.atoms import Atoms

from clinamen.descriptors.descriptors import DescriptorsGenerator
from clinamen.descriptors.descriptors import DescriptorsDatabase


## Create a Si supercell with a Si interstitial
simcell = ase.build.bulk('Si', 'diamond', a=5.434, cubic=True)
supercell = ase.build.supercells.make_supercell(simcell, np.eye(3)*3)
new_symbols = [x for x in supercell.get_chemical_symbols()]
new_symbols.append('Si')
new_cell = supercell.cell.copy()
atoms = supercell.get_scaled_positions()
new_position = np.ones(3)*0.53
new_atoms = np.vstack((atoms, new_position))

# make the defective supercell
system = Atoms(symbols=new_symbols,
               scaled_positions=new_atoms,
               pbc=True, cell=new_cell)


if __name__ == '__main__':
    symbols = system.get_chemical_symbols()
    # structure to write descriptors for
    structures = [system.copy() for i in range(20)]
    for j, atoms in enumerate(structures):
        atoms.rattle(stdev=0.01, seed=j)

    descr_kwargs = {'species': symbols, 'rcut': 4,
                    'nmax': 4, 'lmax': 4, 'periodic': True}

    descriptors_generator = DescriptorsGenerator('SOAP', descr_kwargs)
    database = DescriptorsDatabase('soap_descriptors_flattened.hdf5',
                                   descriptors_generator,
                                   flatten=True)

    database.write_descriptors(structures)

    # create other structures
    additional_structures = [system.copy() for i in range(10)]
    for j, atoms in enumerate(additional_structures):
        atoms.rattle(stdev=0.01, seed=j+100)
    
    database.write_descriptors(additional_structures)


    # read all descriptors in the database
    descriptors, _ = database.read_descriptors()
    print(f'Read descriptors of shape {descriptors.shape}')


    ## same but keep descriptor shape
    descriptors_generator = DescriptorsGenerator('SOAP', descr_kwargs)
    database = DescriptorsDatabase('soap_descriptors.hdf5',
                                   descriptors_generator,
                                   flatten=False)

    database.write_descriptors(structures)
    database.write_descriptors(additional_structures)
    # read all descriptors in the database
    descriptors, _ = database.read_descriptors()
    print(f'Read descriptors of shape {descriptors.shape}')


