# -*- coding: utf-8 -*-
""" Copyright 2020 Marco Arrigoni

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
import time

from ase.calculators.kim import KIM
import ase.build
from ase.optimize.bfgslinesearch import BFGSLineSearch as BFGS

from clinamen.evpd.core.individual import Individual
from clinamen.cmaes.population_evolver import RSPopulationEvolverGrad
from clinamen.cmaes.population_evolver import AnalizeRun
from clinamen.cmaes.evolution import TerminationCriteria, StrategyParameters


## Create the founder
simcell = ase.build.bulk('Si', 'diamond', a=5.434, cubic=True)

# build supercell
supercell = ase.build.supercells.make_supercell(simcell, np.eye(3)*3)

# make interstitial
new_symbols = [x for x in supercell.get_chemical_symbols()]
new_symbols.append('Si')
new_cell = supercell.cell.copy()
atoms = supercell.get_scaled_positions()
# defect position
new_position = np.ones(3)*0.53
new_atoms = np.vstack((atoms, new_position))

# make the founder
founder = Individual(symbols=new_symbols,
                    scaled_positions=new_atoms,
                    pbc=True, cell=new_cell)
founder.my_name = 'Founder'
founder.defect_position = new_position  # set where the defective site is

# Prepare the parameters for initializing the calculator
calc_name = 'Sim_LAMMPS_ModifiedTersoff_PurjaPunMishin_2017_Si__SM_184524061456_000'
params = {'model_name': calc_name}
# attach the calculator to the founder
founder.set_calculator_factory(KIM, params)

initial_energy = founder.get_total_energy()
founder_clone = founder.clone()
founder_clone.optimize_structure(optimizer=BFGS)  # relax the founder structure
founder_clone.write_poscar()  # write POSCAR of the relaxed founder
local_minimum = founder_clone.get_total_energy()


## initialization EA parameters
c_a = 0.6  # coefficient for the gradient
c_cutoff = 4  # hard cut off
c_r = 4  # coefficient for initializing the covariance matrix
step_size = 0.08  # global step-size
random_seed = 10
evolver = RSPopulationEvolverGrad(c_a, c_cutoff, c_r, founder=founder,
                                  step_size=step_size, random_seed=random_seed)

# initializing number of degrees of freedom and teermination criteria
strategy_params = StrategyParameters(len(founder.chromosome.flatten()))
evolver.set_strategy_parameters(strategy_params)
terminator = TerminationCriteria(maxiter=200, smallstd=0.05)
evolver.set_termination_criteria(terminator)
# Only atoms within c_cutoff contribute to the number of degrees of freedom 
evolver.use_reduced_population_size = True

# start the evolution
epoch = 10  # write some files only each given number of generations

print('STARTING THE EVOLUTIONARY PROCESS...')
print('ATOMS WITHIN CUTOFF: ', evolver.atoms_within_cutoff)

start = time.time()

analizer = AnalizeRun(evolver)
analizer.initialize()
try:
    for j, df in enumerate(analizer.evolve()):
        pop = evolver.population
        fitness = pop.individuals_fitness
        index = np.argsort(fitness)
        best_ind = pop[index[-1]]
        count = j + 1
        print(df.iloc[-1], flush=True)
        if count % epoch == 0 or count == 1:  # save the CMA-ES status and the
                                              # individual with the best fitness
            evolver.cmaes.save_status()
            best_ind.write_poscar()
except RuntimeError as e:
    print('\n!!! Error: {} !!!\n'.format(e))
end = time.time()
print(f'ELAPSED TIME: {end - start:.5f} s')

print('RELAXING CONVERGED SOLUTION...')
best_ind.optimize_structure(optimizer=BFGS)  # relax converged solution
best_ind.my_name += '_OPT'
best_ind.write_poscar()
print('{:35s} {:.4f}'.format('Initial energy (eV): ',initial_energy))
print('{:35s} {:.4f}'.format('Energy local minimum (eV): ', local_minimum))
print('{:35s} {:.4f}'.format('Energy converged solution (eV): ', best_ind.get_total_energy()))

