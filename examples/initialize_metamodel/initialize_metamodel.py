# -*- coding: utf-8 -*-
""" Copyright 2020 Marco Arrigoni

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
import os
import h5py

from ase.atoms import Atoms

from clinamen.metamodel.metamodel import BasePCAExactGPMetaModel
from clinamen.descriptors.descriptors import DescriptorsGenerator
from clinamen.descriptors.descriptors import DescriptorsDatabase


rs = RandomState(MT19937(SeedSequence(1231223123)))

# read the structures and energies
with h5py.File('si_ffcs_run2_Xy_train.hdf5', 'r') as f:
    cell = np.array(f['system']['cell'])
    numbers = np.array(f['system']['atomic_numbers'])
    pbc = np.array(f['system']['pbc'])
    no_atoms = len(numbers)
    X = np.array(f['data']['X'])
    y = np.array(f['data']['y'])

# generate corresponding ase atoms
structures = []
for i in range(len(y)):
    pos = X[i].reshape((no_atoms, 3))
    atoms = Atoms(positions=pos, numbers=numbers, pbc=pbc, cell=cell)
    structures.append(atoms)

indices = np.arange(len(structures))
rs.shuffle(indices)


if __name__ == '__main__':
    N_valid = 500  # structures in the validation set
    train_structures = [structures[i] for i in indices[N_valid:]]
    test_structures = [structures[i] for i in indices[:N_valid]]
    y_train = y[N_valid:]
    y_test = y[:N_valid]

    # prepare descriptors database
    symbols = structures[0].get_chemical_symbols()
    descr_kwargs = {'species': symbols, 'rcut': 4,
                    'nmax': 4, 'lmax': 4, 'periodic': True}
    descriptors_generator = DescriptorsGenerator('SOAP', descr_kwargs)
    database = DescriptorsDatabase('soap_descriptors.hdf5',
                                   descriptors_generator,
                                   batch_size=6000)
    # prepare metamodel
    scaler_kwargs = dict(with_std=False)
    pca_kwargs = dict(n_components=0.95)
    metamodel = BasePCAExactGPMetaModel(database, scaler_kwargs, pca_kwargs)

    metamodel.fit(train_structures, y_train, verbose=True)

    mean, std = metamodel.predict(test_structures)

    rmses = np.sqrt(np.power(mean - y_test, 2))
    np.savetxt('rmses.txt', rmses)
    print(f'RMSE = {np.sqrt(np.mean(np.power(mean - y_test, 2)))}')
