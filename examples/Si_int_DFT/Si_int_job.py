# -*- coding: utf-8 -*-
""" Copyright 2020 Marco Arrigoni

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
import os

from mpi4py import MPI

import ase.build
from ase.calculators.vasp import Vasp

from clinamen.evpd.core.individual import Individual
from clinamen.cmaes.population_evolver import RSPopulationEvolverGrad, AnalizeRun
from clinamen.cmaes.evolution import TerminationCriteria, StrategyParameters
from clinamen.cmaes.fitness_calculators import write_train_hdf5


my_rank = MPI.COMM_WORLD.Get_rank()

if __name__ == '__main__':
    ## Specify the calculator parameters
    # CHANGE ACCORDINGLY TO YOUR VASP INSTALLATION
    vasp_exec = '/opt/ohpc/pub/vasp/bin/vasp_gam'
    vasp_cmd = 'prun ' + vasp_exec + ' 1>vasp.out 2>vasp.err'
    incar_params = {'command': vasp_cmd, 'xc': 'lda', 'lwave': True,
                    'setups': 'minimal', 'addgrid': True, 'encut': 307.1,
                    'prec': 'accurate', 'ediff': 1e-3,
                    'ismear': 0, 'sigma': 0.01, 'lscalu': False, 'lplane': True,
                    'lreal': False, 'npar': 8, 'gamma': True, 'kpts': [1, 1, 1]}


    ## Create the founder
    pos_def = np.array([0.6332521153185382, 0.6185839569765310, 0.3698019833234530])

    simcell = ase.build.bulk('Si', 'diamond', a=5.434, cubic=True)
    # build supercell
    supercell = ase.build.supercells.make_supercell(simcell, np.eye(3)*2)
    # make interstitial
    new_symbols = [x for x in supercell.get_chemical_symbols()]
    new_symbols.append('Si')
    new_cell = supercell.cell.copy()
    atoms = supercell.get_scaled_positions()
    # defect position
    new_position = pos_def
    new_atoms = np.vstack((atoms, new_position))
    # make founder
    founder = Individual(symbols=new_symbols,
                        scaled_positions=new_atoms,
                        pbc=True, cell=new_cell)
    founder.my_name = 'Founder'
    founder.defect_position = new_position  # set where the defective site is
    founder.set_calculator_factory(Vasp, incar_params)

    ## Specify CMAES initial and termination parameters
    strategy_params = StrategyParameters(len(founder.chromosome.flatten()))
    terminator = TerminationCriteria(maxiter=300, smallstd=0.05)
    # CMAES params
    c_a = 0.30
    c_cutoff = 4
    c_r = 10
    sigma = 0.08
    runno = 10

    # During the EA run we save some data points 
    # which can eventually be used to train a ML model
    dataset = 'Xy_train.hdf5'
    with open('cmaes_output.txt', 'w') as out_file:
        if my_rank == 0:
            out_file.write('Starting CMAES for point defect\n')
            out_file.flush()
        evolver = RSPopulationEvolverGrad(c_a, c_cutoff, c_r,
                                          founder=founder, step_size=sigma,
                                          random_seed=runno)
        evolver.set_strategy_parameters(strategy_params)
        evolver.set_termination_criteria(terminator)
        evolver.use_reduced_population_size = True
        if my_rank == 0:
            out_file.write('Initial fitness: {}\n'.format(str(evolver.founder.fitness)))
            evolver.founder.write_poscar()
            out_file.write('ATOMS WITHIN CUTOFF: {}\n'.format(str(evolver.atoms_within_cutoff)))
            out_file.flush()
            if not os.path.exists('status'):
                os.makedirs('status')
        analizer = AnalizeRun(evolver)
        analizer.initialize()
        try:
            for j, df in enumerate(analizer.evolve()):
                if my_rank == 0:
                    out_file.write(str(df.iloc[-1]))
                    out_file.write('\n')
                    out_file.flush()
                    evolver.cmaes.save_status('status')
                    pop = evolver.population
                    fitness = pop.individuals_fitness
                    index = np.argsort(fitness)
                    pop[index[-1]].write_poscar('status')
                    # prepare data to be written on the HDF5 file
                    energies = -np.array(fitness)
                    forces = [x.get_forces().ravel() for x in pop]
                    forces = np.vstack(forces)
                    write_train_hdf5(dataset, pop, energies, forces, 'Si_int')
            if my_rank == 0:
                df.to_pickle(os.path.join('status', 'summary_df.pkl'))
        except RuntimeError as e:
            if my_rank == 0:
                out_file.write('\n!!! Error: {}\n'.format(e))
                out_file.flush()
