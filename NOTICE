Clinamen
Copyright (c) 2020 Marco Arrigoni.
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


This product includes the following libraries:

- NumPy: https://www.numpy.org Copyright (c) 2005-2020, NumPy Developers. Released under the terms of the BSD 3-Clause License (Revised).
- SciPy: https://www.scipy.org Copyright (c) 2001, 2002, Enthought, Inc, Copyright (c) 2003-2020, SciPy Developers. Released under the terms of the BSD 3-Clause License (Revised).
- Pandas: https://pandas.pydata.org Copyright (c) 2008-2011, AQR Capital Management, LLC, Lambda Foundry, Inc. and PyData Development Team, Copyright (c) 2011-2020, Open source contributors. Released under the terms of the BSD 3-Clause License (Revised).
- scikit-learn: https://scikit-learn.org Copyright (c) 2007-2020, The scikit-learn developers. Released under the terms of the BSD 3-Clause License (Revised).
- Cython: https://cython.org Copyright (c) 2009-2020, The Cython developers. Released under the terms of the Apache License, Version 2.0.
- mpi4py: https://mpi4py.readthedocs.io Copyright (c) 2013, Lisandro Dalcin. Released under the terms of the BSD License (BSD).
- h5py: https://www.h5py.org Copyright (c) 2008, Andrew Collette and contributors. Released under the terms of the BSD 3-Clause License (Revised).
- ASE: https://wiki.fysik.dtu.dk/ase Copyright (c) 2017, ASE-developers. Released under the terms of the GNU Lesser General Public License, Version 2.
- PyTorch: https://pytorch.org Copyright (c) PyTorch developers. Relased under the terms of the BSD License.
- GPyTorch: https://https://gpytorch.ai Copyright (c) 2017, Jake Gardner. Relased under the terms of the MIT License.
- DScribe: https://https://singroup.github.io/dscribe/latest/ Copyright (c) DScribe developers. Released under the terms of the Apache License, Version 2.0.
