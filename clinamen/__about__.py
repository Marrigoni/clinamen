# -*- coding: utf-8 -*-
""" Copyright 2020 Marco Arrigoni

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
__all__ = ['__author__',
           '__email__',
           '__title__',
           '__version__',
           '__summary__',
           '__uri__',
           '__copyright__',
           '__license__'
          ]

__author__ = 'Marco Arrigoni'

__email__ = 'marco.arrigoni@outlook.de'

__title__ = 'clinamen'

__version__ = '0.2.0a3'

__summary__ = ('Clinamen: a Python package for the first-principles '
               'systematic discovery of low-energy configurations in '
               'defective systems.')

__uri__ = 'https://gitlab.com/Marrigoni/clinamen'

__copyright__ = '2020'

__license__ = 'Apache License, Version 2.0'

