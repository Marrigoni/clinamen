The :program:`Clinamen` package
*******************************

**Clinamen**: Covariance-matrix-adaptation evolution strategy for Localized
Impurities with unsupervised Machine learning Exploration of Non-elementary
structures.

**Clinamen** is a Python package implementing the covariance-matrix-adaptaion
evolution strategy (CMA-ES) :cite:`Hansen-2001` for finding low-energy configurations of localized
defects. It additionally employs an unsupervised machine learning (ML) approach
for exploiting the structures visited by the evolutionary algorithm (EA) in order
to find additional low-energy minima.


The meaning of the word `clinamen`
----------------------------------
`Clinamen` is the name that the Roman philosopher and poet Titus Lucretius Carus gave to the
random deviations in the rectilinear motion of atoms, which, according to the Epicurean atomistic theory,
are responsible for their collisions and aggregation into objects.

In the `De Rerum Natura`, Lucretius also presents a theory of natural selection based on the random
generation, by the Earth, of creatures of any sort, among which only the most fit ones are able to survive and reproduce.

The importance of the random generation of individuals and of fitness-based selection in the doctrine of Lucretius, together
with the fact that `clinamen` can be used as an acronym describing the main ingredients of the method 
implemented in this package, are the main reasons for the choice of this name.
