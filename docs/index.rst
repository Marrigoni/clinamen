Welcome to the documentation of the Clinamen package!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   general
   installation
   case_studies
   releasenotes
   api
   bibliography
   contacts
