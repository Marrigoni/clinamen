API reference
*************

.. automodule:: clinamen
   :members:

Stand-alone implementation of the CMA-ES
========================================

- :mod:`clinamen.cmaes.evolution`

- :mod:`clinamen.cmaes.fitness_calculators`

- :mod:`clinamen.cmaes.population_evolver`

The :mod:`evolution` module
---------------------------

.. automodule:: clinamen.cmaes.evolution
   :members:

The :mod:`fitness_calculators` module
-------------------------------------

.. automodule:: clinamen.cmaes.fitness_calculators
   :members:

The :mod:`population_evolver` module
------------------------------------

.. automodule:: clinamen.cmaes.population_evolver
   :members:

Objects describing the genotype of individuals and their populations
====================================================================

- :mod:`clinamen.evpd.core`

The :mod:`evpd.core.individual` module
--------------------------------------

.. automodule:: clinamen.evpd.core.individual
   :members:

The :mod:`evpd.core.population` module
--------------------------------------

.. automodule:: clinamen.evpd.core.population
   :members:

Crystal structure fingerprint descriptors and utilities for general descriptors
===============================================================================

- :mod:`clinamen.descriptors.descriptors_cython`
- :mod:`clinamen.descriptors.utils`

The :mod:`descriptors_cython` module
------------------------------------

.. automodule:: clinamen.descriptors.descriptors_cython
   :members:

The :mod:`descriptors.utils` module
-----------------------------------

.. automodule:: clinamen.descriptors.utils
   :members:

Utilities for the unsupervised classification of clusters
=========================================================

- :mod:`clinamen.clustering.misc`
- :mod:`clinamen.clustering.stats_tools`

The :mod:`clustering.misc` module
---------------------------------

.. automodule:: clinamen.clustering.misc
   :members:

The :mod:`clustering.stats_tools` module
----------------------------------------

.. automodule:: clinamen.clustering.stats_tools
   :members:

Objects representing the metamodel 
==================================

- :mod:`clinamen.metamodel.metamodel`

The :mod:`metamodel` module
---------------------------

.. automodule:: clinamen.metamodel.metamodel
   :members:

