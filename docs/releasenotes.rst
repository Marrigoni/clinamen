.. _release_notes:

Release Notes
*************

Version 0.2.0
=============

04 February 2021:

- GP regressor backend moved to ``GPyTorch``.
- Interface with ``DScribe`` library for calculating materials descriptors
 in an unified way.

Version 0.1.0
=============

16 October 2020:

- First packaged version of the code.

