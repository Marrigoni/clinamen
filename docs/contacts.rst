Contact
*******

E-mail
------
The developers of :program:`Clinamen` can be reached through these e-mail addresses:

    - marco.arrigoni@outlook.de, marco.arrigoni@tuwien.ac.at

Gitlab
------
:program:`Clinamen` is hosted on Gitlab on the following web page:

    - https://gitlab.com/Marrigoni/clinamen
