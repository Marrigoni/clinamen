# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

# Mock packages requiring C libraries and/or extension modules
from unittest.mock import MagicMock

class Mock(MagicMock):
    @classmethod
    def __getattr__(cls, name):
        return MagicMock()

MOCK_MODULES = ['mpi4py', 
                'torch', 
                'gpytorch',
                'dscribe'
                ]

sys.modules.update((mod_name, Mock()) for mod_name in MOCK_MODULES)

autodoc_mock_imports = MOCK_MODULES

sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(1, os.path.abspath(os.path.join('..', 'clinamen', 'cmaes')))
sys.path.insert(1, os.path.abspath(os.path.join('..',
                                                'clinamen', 'clustering')))
sys.path.insert(1, os.path.abspath(os.path.join('..',
                                               'clinamen', 'metamodel')))
sys.path.insert(1, os.path.abspath(os.path.join('..',
                                                'clinamen', 'descriptors')))
sys.path.insert(1, os.path.abspath(os.path.join('..', 'clinamen', 'evpd')))

# -- Project information -----------------------------------------------------

project = 'Clinamen'
copyright = '2020, Marco Arrigoni'
author = 'Marco Arrigoni'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.doctest', 'sphinx.ext.todo',
              'sphinx.ext.coverage', 'sphinx.ext.mathjax', 'sphinx.ext.ifconfig',
              'sphinx.ext.intersphinx', 'sphinx.ext.viewcode', 'sphinx.ext.autosummary',
              'sphinx.ext.napoleon', 'sphinx.ext.graphviz', 'sphinxcontrib.bibtex'
             ] 
# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []


# Read some variables concerning the description of the Spinney package
with open(os.path.join('..', 'clinamen', '__about__.py'), 'r') as f:
    for line in f:
        line = line.strip()
        if line.startswith('__'):
            processed_line = line.split('=')
            labline = processed_line[0].strip()
            dataline = processed_line[1].strip()
            if labline == '__author__':
                author = dataline
            elif labline == '__email__':
                email = dataline
            elif labline == '__version__':
                version = dataline
                release = version
            elif labline == '__uri__':
                uri = dataline
            elif labline == '__copyright__':
                copyright = dataline
            elif labline == '__license__':
                license = dataline

html_context = {'current version' : version}

# numerated figures
numfig = True
