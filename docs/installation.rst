Installation
************

Setup
=====
The code is available on the `Python Package Index <https://pypi.org/>`_.
The easiest way to install :program:`Clinamen` is through `pip`:

::
    
    pip install clinamen

We suggest to create a virtual environment in which the **Clinamen** code can be installed.

The requirements for a successful installation are:

 - `Python` version 3.8 or newer;
 - `NumPy <https://www.numpy.org>`_ version  1.18 or newer;
 - `SciPy <https://www.scipy.org>`_ version 1.5 or newer;
 - `Pandas <https://pandas.pydata.org/>`_ version 1.0 or newer;
 - `scikit-learn <https://scikit-learn.org/>`_ version 0.23 or newer;
 - `Cython <https://cython.org/>`_ version 0.29 or newer;
 - `mpi4py <https://mpi4py.readthedocs.io/>`_ version 3.0 or newer;
 - `h5py <https://www.h5py.org/>`_ version 2.10 or newer;
 - `Atomic Simulation Environment <https://wiki.fysik.dtu.dk/ase>`_ version 3.19 or newer;
 - `PyTorch <https://pytorch.org/>`_ version 1.7 or newer;
 - `GPyTorch <https://gpytorch.ai/>`_ version 1.3 or newer;
 - `DScribe <https://singroup.github.io/dscribe/latest/>`_ version 0.4 or newer. 

It might be that some packages cannot be installed directly with the **Clinamen** package using `pip`
(for example `PyTorch` installation could fail in this way). For this reason these packages have to
be installed manually before installing **Clinamen** with `pip`. 


The :program:`Clinamen` package can also be installed from source from the online
`Gitlab repository <https://gitlab.com/Marrigoni/clinamen>`_.

To install the :program:`Clinamen` package, create a new directory and
clone the repository. From the root directory, run the command:

::

    python3 setup.py build_ext --inplace

Export the package's top directory to the ``PYTHONPATH`` environmental variable.
Now the package can be imported and used.
